package com.krd.mini_project2.model;

public class User {
    private String user;
    private String email;
    private String photoUrl;
    private String userId;
    public User(){

    }
    public User(String user){
        this.user = user;

    }

    public User(String user, String email, String photoUrl, String userId) {
        this.user = user;
        this.email = email;
        this.photoUrl = photoUrl;
        this.userId = userId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "User{" +
                "user='" + user + '\'' +
                ", email='" + email + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
