package com.krd.mini_project2.directionherlpers;

public interface TaskLoadedCallback {

    void onTaskDone(Object... values);
}
